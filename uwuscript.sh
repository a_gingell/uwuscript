#!/bin/bash

DIR=${HOME}/.uwuscript
CAT=$DIR/uwu_$(cat $DIR/uwu_categories.txt | dmenu).txt
if [ -f $CAT ]; then
    EMOTE=$(cat $CAT | dmenu )
    #echo $(cat $CAT | dmenu ) | xclip -selection c
    xdotool type --delay 50 "$EMOTE"
else
    echo "ERROR: File not found."
    touch $CAT
fi
